package com.appsofsoumyadeep.yournotes.Activities;

import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.appsofsoumyadeep.yournotes.Model.Notes;
import com.appsofsoumyadeep.yournotes.R;
import com.appsofsoumyadeep.yournotes.UI.NotesRecyclerViewAdapter;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator;

public class MainActivity extends AppCompatActivity implements FirebaseAuth.AuthStateListener, NotesRecyclerViewAdapter.NoteListener {

    public static final int REQUEST_CODE_ADD_NOTE = 1;
    public static final int REQUEST_CODE_UPDATE_NOTE = 2;
    private static final String TAG = "MainActivity";
    int flag = 0;
    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper
            .SimpleCallback(0, ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            if (direction == ItemTouchHelper.LEFT) {
                NotesRecyclerViewAdapter.NoteViewHolder
                        noteViewHolder = (NotesRecyclerViewAdapter.NoteViewHolder) viewHolder;
                noteViewHolder.swipeDeleteNote();
            }
        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

            new RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                    .addBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.red))
                    .addActionIcon(R.drawable.ic_delete)
                    .create()
                    .decorate();

            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }
    };
    private ImageButton addnote;
    private EditText inputSearch;
    private RecyclerView recyclerView;
    private NotesRecyclerViewAdapter notesRecyclerViewAdapter;
    private Timer timer;
    private int noteClickedPostion = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.notesRecyclerView);
        inputSearch = (EditText) findViewById(R.id.inputSearch);

        recyclerView.setLayoutManager(
                new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        );

        addnote = (ImageButton) findViewById(R.id.imageAddNoteMain);
        addnote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, CreateNoteActivity.class));
            }
        });

        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                cancelTimer();
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()) {
                    initRecyclerView(FirebaseAuth.getInstance().getCurrentUser());
                } else if (notesRecyclerViewAdapter.getItemCount() != 0)
                    searchNotes(s.toString());
            }
        });

    }

    private void startLoginActivity() {
        startActivity(new Intent(this, LoginRegisterActivity.class));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.action_logout:
                Toast.makeText(this, "Logout", Toast.LENGTH_SHORT).show();
                AuthUI.getInstance().signOut(this);
                return true;
            case R.id.action_profile:
                Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseAuth.getInstance().addAuthStateListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        FirebaseAuth.getInstance().removeAuthStateListener(this);
        if (notesRecyclerViewAdapter != null) {
            notesRecyclerViewAdapter.stopListening();
        }
    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        if (firebaseAuth.getCurrentUser() == null) {
            startLoginActivity();
            return;
        }

        initRecyclerView(firebaseAuth.getCurrentUser());
    }

    private void initRecyclerView(FirebaseUser user) {
        recyclerView.setLayoutManager(
                new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        );

        Query query = FirebaseFirestore.getInstance()
                .collection("users")
                .document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .collection("notes");

        FirestoreRecyclerOptions<Notes> options = new FirestoreRecyclerOptions.Builder<Notes>()
                .setQuery(query, Notes.class)
                .build();
        notesRecyclerViewAdapter = new NotesRecyclerViewAdapter(options, this);
        recyclerView.setAdapter(notesRecyclerViewAdapter);
        notesRecyclerViewAdapter.startListening();
        Log.d(TAG, "initRecyclerView: " + notesRecyclerViewAdapter.getItemCount());
        recyclerView.smoothScrollToPosition(0);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);

    }

    public void searchNotes(final String searchKeyword) {

        Query query = FirebaseFirestore.getInstance()
                .collection("users")
                .document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .collection("notes");

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (!searchKeyword.trim().isEmpty()) {
                    query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (DocumentSnapshot documentSnapshot : Objects.requireNonNull(task.getResult())) {
                                    if (documentSnapshot.get("titleNotes") != null) {
                                        if (documentSnapshot.get("titleNotes").toString().contains(searchKeyword)
                                                || documentSnapshot.get("commentNotes").toString().contains(searchKeyword)
                                                || documentSnapshot.get("descriptionNotes").
                                                toString().contains(searchKeyword)) {

                                            Query querya = query
                                                    .whereEqualTo("noteID", documentSnapshot.getId());

                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    FirestoreRecyclerOptions<Notes> options =
                                                            new FirestoreRecyclerOptions.Builder<Notes>()
                                                                    .setQuery(querya, Notes.class)
                                                                    .build();

                                                    notesRecyclerViewAdapter.updateOptions(options);
                                                    recyclerView.setAdapter(notesRecyclerViewAdapter);
                                                    notesRecyclerViewAdapter.startListening();
                                                    recyclerView.scrollToPosition(0);
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    });

                }

            }
        }, 150);
    }

    public void cancelTimer() {
        if (timer != null) {
            timer.cancel();
        }
    }

    @Override
    public void viewNotes(DocumentSnapshot snapshot) {

        Notes note = snapshot.toObject(Notes.class);

        Intent intent = new Intent(getApplicationContext(), CreateNoteActivity.class);
        intent.putExtra("isViewOrUpdate", true);
        intent.putExtra("note", note);
        startActivityForResult(intent, REQUEST_CODE_UPDATE_NOTE);
    }

    @Override
    public void swipeDeleteNote(DocumentSnapshot snapshot) {

        final DocumentReference documentReference = snapshot.getReference();
        final Notes notes = snapshot.toObject(Notes.class);

        documentReference.delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });

        Snackbar.make(recyclerView, "Note Deleted", Snackbar.LENGTH_LONG)
                .setAction("Undo", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        documentReference.set(notes);
                    }
                })
                .show();

    }
}