package com.appsofsoumyadeep.yournotes.Utility;

import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.appsofsoumyadeep.yournotes.Model.Notes;
import com.appsofsoumyadeep.yournotes.UI.NotesRecyclerViewAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;
import java.util.Map;

public class DataBaseHandler{
    private static final String TAG = "DataBaseHandler";
    private FirebaseFirestore fireStore;

    public DataBaseHandler(){
    }

    public void CreateNote(Notes note)
    {
        fireStore = FirebaseFirestore.getInstance();
        DocumentReference documentReference = fireStore.collection("users")
                .document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .collection("notes").document();

        note.setNoteID(documentReference.getId());

                documentReference
                .set(note)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
    }

    public void UpdateNote(Notes note) {

        fireStore = FirebaseFirestore.getInstance();

        Map<String, Object> notes = new HashMap<>();
        notes.put("colourNotes",note.getColourNotes());
        notes.put("commentColor",note.getCommentColor());
        notes.put("commentNotes",note.getCommentNotes());
        notes.put("dateColor",note.getDateColor());
        notes.put("descriptionNotes",note.getDescriptionNotes());
        notes.put("imageNotes",note.getImageNotes());
        notes.put("timeNotesCreated",note.getTimeNotesCreated());
        notes.put("titleColor",note.getTitleColor());
        notes.put("titleNotes",note.getTitleNotes());
        notes.put("userID",note.getUserID());
        notes.put("noteID",note.getNoteID());

        DocumentReference documentReference = fireStore.collection("users")
                .document(note.getUserID())
                .collection("notes")
                .document(note.getNoteID());

        documentReference.update(notes)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
    }

    public void deleteImage(String imageURL){
        StorageReference photoRef = FirebaseStorage.getInstance().getReferenceFromUrl(imageURL);
        photoRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
            }
        });
    }

    public void deleteNote(Notes note) {
        fireStore = FirebaseFirestore.getInstance();
        DocumentReference documentReference = fireStore.collection("users")
                .document(note.getUserID())
                .collection("notes")
                .document(note.getNoteID());

        documentReference.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }

//    public void Search(Notes note ,String keyword)
//    {
//        fireStore = FirebaseFirestore.getInstance();
//        Query querySearch = fireStore.collection("users")
//                .document(note.getUserID())
//                .collection("notes");
//
//        querySearch.startAt("title", keyword)
//                .endAt("title", keyword+"\uf8ff");
//
//        FirestoreRecyclerOptions<Notes> options = new FirestoreRecyclerOptions.Builder<Notes>()
//            .setQuery(querySearch, Notes.class)
//            .build();
//
//        new NotesRecyclerViewAdapter(options, new NotesRecyclerViewAdapter.NoteListener() {
//            @Override
//            public void viewNotes(DocumentSnapshot snapshot) {
//            }
//        });
//    }
}
