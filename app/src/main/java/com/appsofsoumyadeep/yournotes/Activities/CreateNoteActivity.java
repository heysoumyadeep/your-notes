package com.appsofsoumyadeep.yournotes.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.ImageViewCompat;

import com.appsofsoumyadeep.yournotes.Model.Notes;
import com.appsofsoumyadeep.yournotes.R;
import com.appsofsoumyadeep.yournotes.Utility.DataBaseHandler;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class CreateNoteActivity extends AppCompatActivity {

    private EditText inputNoteTitle, inputNoteComment, inputNoteDescription;
    private TextView dateOfCreation, webLink;
    private ImageView backButton, saveButton, saveButtonBorder, imageNotes, imageCrossButton;
    private StorageReference storageReference;
    private DatabaseReference mPostDatabase;
    private Uri imageUri;
    private View viewComment;
    private String selectedNoteColor, titleColor, commentColor, dateColor;
    private DataBaseHandler db;
    private static final int REQUEST_CODE = 1;
    private static final int REQUEST_CODE_SELECT_IMAGE = 2;
    private Notes viewNote;
    private AlertDialog dialogDeleteNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_note);

        db = new DataBaseHandler();

        backButton = (ImageView) findViewById(R.id.imageBack);
        saveButton = (ImageView) findViewById(R.id.imageSave);
        saveButtonBorder = (ImageView) findViewById(R.id.saveButtonBorder);
        inputNoteTitle = (EditText) findViewById(R.id.inputNoteTitle);
        inputNoteComment = (EditText) findViewById(R.id.inputNoteComment);
        inputNoteDescription = (EditText) findViewById(R.id.inputNoteDescription);
        dateOfCreation = (TextView) findViewById(R.id.noteDate);
        viewComment = (View) findViewById(R.id.viewComment);
        imageNotes = (ImageView) findViewById(R.id.imageNote);
        imageCrossButton = (ImageView) findViewById(R.id.imageCrossButton);

        storageReference = FirebaseStorage.getInstance().getReference();

        mPostDatabase = FirebaseDatabase.getInstance().getReference().child("NotesImage");

        if (getIntent().getBooleanExtra("isViewOrUpdate", false)) {
            viewNote = (Notes) getIntent().getSerializableExtra("note");
            setViewOrUpdateNote();
        } else {
            String noteDate = new SimpleDateFormat("EEEE, dd MMMM yyyy HH:mm a", Locale.getDefault())
                    .format(new Date());
            dateOfCreation.setText(noteDate);
        }

        initColourPicker();

        imageCrossButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageUri = null;
                imageNotes.setVisibility(View.GONE);
                imageCrossButton.setVisibility(View.GONE);
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                hideSoftKeyboard(v);
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (inputNoteTitle.getText().toString().trim().isEmpty() &&
                        inputNoteDescription.getText().toString().trim().isEmpty() &&
                        inputNoteComment.getText().toString().trim().isEmpty())
                    Toast.makeText(CreateNoteActivity.this, "GGG", Toast.LENGTH_SHORT).show();
                else {
                    saveNote(v);
                    setResult(RESULT_OK, new Intent());
                    finish();
                }
                hideSoftKeyboard(v);
            }
        });

        imageNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                if (viewNote != null && imageUri == null)
                    intent.setData(Uri.parse(viewNote.getImageNotes()));
                else
                    intent.setDataAndType(imageUri, "image/*");
                startActivity(intent);
            }
        });

    }

    //TODO: Move to Another Class

    private void setViewOrUpdateNote() {
        inputNoteTitle.setText(viewNote.getTitleNotes());
        inputNoteTitle.setSelection(viewNote.getTitleNotes().length());
        inputNoteComment.setText(viewNote.getCommentNotes());
        inputNoteComment.setSelection(viewNote.getCommentNotes().length());
        dateOfCreation.setText("Last Modified: " + viewNote.getTimeNotesCreated());
        inputNoteDescription.setText(viewNote.getDescriptionNotes());
        inputNoteDescription.setSelection(viewNote.getDescriptionNotes().length());
        if (viewNote.getImageNotes() != null && !viewNote.getImageNotes().trim().isEmpty()) {
            Glide.with(imageNotes.getContext())
                    .load(viewNote.getImageNotes())
                    .into(imageNotes);
            imageNotes.setVisibility(View.VISIBLE);
            imageCrossButton.setVisibility(View.VISIBLE);
        }
    }

    protected void hideSoftKeyboard(View v) {
        if (v != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    private void saveNote(View v) {

        Notes note = new Notes();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        String noteDate = new SimpleDateFormat("EEEE, dd MMMM yyyy HH:mm a", Locale.getDefault())
                .format(new Date());

        if (inputNoteTitle.getText().toString().trim().isEmpty()) {
            if (inputNoteDescription.getText().toString().trim().isEmpty()) {
                if (!inputNoteComment.getText().toString().trim().isEmpty()) {
                    note.setTitleNotes("Untitled");
                    note.setDescriptionNotes("");
                    note.setCommentNotes(inputNoteComment.getText().toString());
                    note.setUserID(user.getUid());
                    note.setTimeNotesCreated(noteDate);
                }
            } else {
                if (inputNoteComment.getText().toString().trim().isEmpty())
                    note.setCommentNotes("");
                else
                    note.setCommentNotes(inputNoteComment.getText().toString());

                note.setTitleNotes("Untitled");
                note.setDescriptionNotes(inputNoteDescription.getText().toString());
                note.setUserID(user.getUid());
                note.setTimeNotesCreated(noteDate);
            }
        } else {
            if (inputNoteDescription.getText().toString().trim().isEmpty()) {
                if (inputNoteComment.getText().toString().trim().isEmpty())
                    note.setCommentNotes("");
                else
                    note.setCommentNotes(inputNoteComment.getText().toString());
                note.setDescriptionNotes("");
            } else {
                if (inputNoteComment.getText().toString().trim().isEmpty())
                    note.setCommentNotes("");
                else
                    note.setCommentNotes(inputNoteComment.getText().toString());
                note.setDescriptionNotes(inputNoteDescription.getText().toString());
            }

            note.setTitleNotes(inputNoteTitle.getText().toString());
            note.setUserID(user.getUid());
            note.setTimeNotesCreated(noteDate);
        }

        note.setColourNotes(selectedNoteColor);
        note.setTitleColor(titleColor);
        note.setCommentColor(commentColor);
        note.setDateColor(dateColor);

        if (imageUri != null) {
            StorageReference filepath = storageReference.child("NotesImage").child(user.getUid()).child(imageUri.getLastPathSegment());

            filepath.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    filepath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            note.setImageNotes(uri.toString());
                            if (viewNote != null) {
                                note.setNoteID(viewNote.getNoteID());
                                viewNote = note;
                                db.UpdateNote(viewNote);
                            } else
                                db.CreateNote(note);
                        }
                    });
                }
            });
        } else {
            if (viewNote != null) {
                note.setNoteID(viewNote.getNoteID());
                if (imageNotes.getVisibility() == View.VISIBLE)
                    note.setImageNotes(viewNote.getImageNotes());
                else if (viewNote.getImageNotes() != null)
                    db.deleteImage(viewNote.getImageNotes());

                viewNote = note;
                db.UpdateNote(viewNote);
            } else
                db.CreateNote(note);
        }
    }

    private void initColourPicker() {
        final LinearLayout layoutColourPicker = findViewById(R.id.layoutColourPicker);
        final BottomSheetBehavior<LinearLayout> bottomSheetBehavior = BottomSheetBehavior.from(layoutColourPicker);
        layoutColourPicker.findViewById(R.id.textNoteColourPicker).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }
        });

        final ImageView[] noteColorImage = new ImageView[8];

        final int[] imageColorID = {R.id.imageColor1,
                R.id.imageColor2,
                R.id.imageColor3,
                R.id.imageColor4,
                R.id.imageColor5,
                R.id.imageColor6,
                R.id.imageColor7,
                R.id.imageColor8};

        final int[] viewColorID = {
                R.id.viewColor1,
                R.id.viewColor2,
                R.id.viewColor3,
                R.id.viewColor4,
                R.id.viewColor5,
                R.id.viewColor6,
                R.id.viewColor7,
                R.id.viewColor8,
        };

        final String[] colorTheme = {"#FFC107", "#F44336", "#009635", "#7C00CF", "#3F51B5",
                "#FFFFFF", "#B3B3B3", "#000000"};

        final String[] colorTitle = {"#4d3b05", "#ffeceb", "#c2fcd7", "#f2deff", "#d6ddff",
                "#40413d", "#181818", "#e1e1e1"};

        final String[] colorComment = {"#735702", "#fcc8c5", "#91ebb0", "#e9c7ff", "#b6c0ee",
                "#6a6c68", "#2c2c2c", "#c9c9c9"};

        final String[] colorDate = {"#967309", "#ffb1ab", "#5ee08c", "#e1b3ff", "#a4afe0",
                "#919191", "#525252", "#929292"};

        Map<String, List<String>> map = new HashMap<>(8);

        for (int i = 0; i < noteColorImage.length; i++) {
            noteColorImage[i] = layoutColourPicker.findViewById(imageColorID[i]);

            List<String> noteColours = new ArrayList<>();
            noteColours.add(colorTitle[i]);
            noteColours.add(colorComment[i]);
            noteColours.add(colorDate[i]);

            map.put(colorTheme[i], noteColours);

        }

        for (int i = 0; i < noteColorImage.length; i++) {
            int finalI = i;
            layoutColourPicker.findViewById(viewColorID[i]).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedNoteColor = colorTheme[finalI];
                    noteColorImage[finalI].setVisibility(View.VISIBLE);
                    for (int j = 0; j < noteColorImage.length; j++) {
                        if (finalI == j)
                            continue;
                        noteColorImage[j].setVisibility(View.INVISIBLE);
                    }
                    titleColor = map.get(selectedNoteColor).get(0);
                    commentColor = map.get(selectedNoteColor).get(1);
                    dateColor = map.get(selectedNoteColor).get(2);
                    noteTheme();

                }
            });
        }

        if (viewNote != null && viewNote.getColourNotes() != null && !viewNote.getColourNotes().trim().isEmpty()) {
            for (int i = 0; i < viewColorID.length; i++) {
                if (colorTheme[i].equals(viewNote.getColourNotes()))
                    layoutColourPicker.findViewById(viewColorID[i]).performClick();
            }
        } else {
            layoutColourPicker.findViewById(viewColorID[0]).performClick();
        }

        layoutColourPicker.findViewById(R.id.layoutImageAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                if (ContextCompat.checkSelfPermission(
                        getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(CreateNoteActivity.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            REQUEST_CODE);
                } else {
                    selectImage();
                }
            }
        });

        if (viewNote != null) {
            layoutColourPicker.findViewById(R.id.layoutDeleteNote).setVisibility(View.VISIBLE);
            layoutColourPicker.findViewById(R.id.layoutDeleteNote)
                    .setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                            showDeleteNoteDialogue(viewNote);
                        }
                    });
        }

    }

    private void showDeleteNoteDialogue(Notes note) {
        if (dialogDeleteNote == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(CreateNoteActivity.this);
            View view = LayoutInflater.from(this).inflate(R.layout.layout_delete_note
                    , (ViewGroup) findViewById(R.id.deleteNoteContainer)
            );
            builder.setView(view);
            dialogDeleteNote = builder.create();
            view.findViewById(R.id.buttonDeleteNote).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    db.deleteNote(note);
                    dialogDeleteNote.dismiss();
                    setResult(RESULT_OK, new Intent());
                    finish();
                }
            });
            view.findViewById(R.id.buttonCancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogDeleteNote.dismiss();
                }
            });
        }
        dialogDeleteNote.show();
    }

    private void noteTheme() {

        GradientDrawable gradientDrawable = (GradientDrawable) viewComment.getBackground();
        gradientDrawable.setColor(Color.parseColor(selectedNoteColor));

        switch (selectedNoteColor) {
            case "#FFC107": {
                inputNoteTitle.setTextColor(Color.parseColor("#FFC107"));
                ImageViewCompat.setImageTintList(saveButton, ColorStateList.valueOf(Color.parseColor("#FFC107")));
                ImageViewCompat.setImageTintList(saveButtonBorder, ColorStateList.valueOf(Color.parseColor("#FFC107")));
                inputNoteTitle.setHintTextColor(Color.parseColor("#AA8602"));
                break;
            }

            case "#F44336": {
                inputNoteTitle.setTextColor(Color.parseColor("#ff7369"));
                ImageViewCompat.setImageTintList(saveButton, ColorStateList.valueOf(Color.parseColor("#ff7369")));
                ImageViewCompat.setImageTintList(saveButtonBorder, ColorStateList.valueOf(Color.parseColor("#ff7369")));
                inputNoteTitle.setHintTextColor(Color.parseColor("#ab746f"));
                break;
            }

            case "#009635": {
                inputNoteTitle.setTextColor(Color.parseColor("#1dcf5c"));
                ImageViewCompat.setImageTintList(saveButton, ColorStateList.valueOf(Color.parseColor("#1dcf5c")));
                ImageViewCompat.setImageTintList(saveButtonBorder, ColorStateList.valueOf(Color.parseColor("#1dcf5c")));
                inputNoteTitle.setHintTextColor(Color.parseColor("#5c8068"));
                break;
            }

            case "#7C00CF": {
                inputNoteTitle.setTextColor(Color.parseColor("#d08aff"));
                ImageViewCompat.setImageTintList(saveButton, ColorStateList.valueOf(Color.parseColor("#d08aff")));
                ImageViewCompat.setImageTintList(saveButtonBorder, ColorStateList.valueOf(Color.parseColor("#d08aff")));
                inputNoteTitle.setHintTextColor(Color.parseColor("#886e99"));
                break;
            }

            case "#3F51B5": {
                inputNoteTitle.setTextColor(Color.parseColor("#758afa"));
                ImageViewCompat.setImageTintList(saveButton, ColorStateList.valueOf(Color.parseColor("#758afa")));
                ImageViewCompat.setImageTintList(saveButtonBorder, ColorStateList.valueOf(Color.parseColor("#758afa")));
                inputNoteTitle.setHintTextColor(Color.parseColor("#6c74a3"));
                break;
            }

            case "#FFFFFF": {
                inputNoteTitle.setTextColor(Color.parseColor("#FFFFFF"));
                ImageViewCompat.setImageTintList(saveButton, ColorStateList.valueOf(Color.parseColor("#FFFFFF")));
                ImageViewCompat.setImageTintList(saveButtonBorder, ColorStateList.valueOf(Color.parseColor("#FFFFFF")));
                inputNoteTitle.setHintTextColor(Color.parseColor("#8f8f8f"));
                break;
            }

            case "#B3B3B3": {
                inputNoteTitle.setTextColor(Color.parseColor("#C3C3C3"));
                ImageViewCompat.setImageTintList(saveButton, ColorStateList.valueOf(Color.parseColor("#C3C3C3")));
                ImageViewCompat.setImageTintList(saveButtonBorder, ColorStateList.valueOf(Color.parseColor("#C3C3C3")));
                inputNoteTitle.setHintTextColor(Color.parseColor("#8f8f8f"));
                break;
            }

            case "#000000": {
                inputNoteTitle.setTextColor(Color.parseColor("#bfbfbf"));
                ImageViewCompat.setImageTintList(saveButton, ColorStateList.valueOf(Color.parseColor("#bfbfbf")));
                ImageViewCompat.setImageTintList(saveButtonBorder, ColorStateList.valueOf(Color.parseColor("#bfbfbf")));
                inputNoteTitle.setHintTextColor(Color.parseColor("#8f8f8f"));
                break;
            }
        }
    }

    private void selectImage() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_CODE_SELECT_IMAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                selectImage();
            } else {
                Toast.makeText(this, "Permission Denied!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_SELECT_IMAGE && resultCode == RESULT_OK) {
            if (data != null) {
                imageUri = data.getData();
                if (imageUri != null) {
                    imageNotes.setImageURI(imageUri);
                    imageNotes.setVisibility(View.VISIBLE);
                    imageCrossButton.setVisibility(View.VISIBLE);
                }
            }
        }
    }
}