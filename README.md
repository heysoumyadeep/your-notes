# Your-Notes

## UI Inspired from

![11123324-Notes-App](https://cdn.dribbble.com/users/2307524/screenshots/11123324/media/427ded4f0ce6bba47009129547a04b32.jpg)

and

![13966457-Mobile-Notes-App](https://cdn.dribbble.com/users/844462/screenshots/13966457/media/c46cc2fbb48d4006add6f76d469a3c01.png)

Currently this app supports Dark Mode only and avaiable only on Android.

---

