package com.appsofsoumyadeep.yournotes.Model;

import java.io.Serializable;

public class Notes implements Serializable {
    private String titleNotes;
    private String timeNotesCreated;
    private String commentNotes;
    private String descriptionNotes;
    private String imageNotes;
    private String colourNotes;
    private String userID;
    private String noteID;

    private String titleColor, commentColor, dateColor;

    public Notes(String titleNotes, String timeNotesCreated, String commentNotes, String descriptionNotes, String imageNotes, String colourNotes, String userID, String noteID, String titleColor, String commentColor, String dateColor) {
        this.titleNotes = titleNotes;
        this.timeNotesCreated = timeNotesCreated;
        this.commentNotes = commentNotes;
        this.descriptionNotes = descriptionNotes;
        this.imageNotes = imageNotes;
        this.colourNotes = colourNotes;
        this.userID = userID;
        this.noteID = noteID;
        this.titleColor = titleColor;
        this.commentColor = commentColor;
        this.dateColor = dateColor;
    }

    public Notes() {
    }

    public void setTitleColor(String titleColor) {
        this.titleColor = titleColor;
    }

    public void setCommentColor(String commentColor) {
        this.commentColor = commentColor;
    }

    public void setDateColor(String dateColor) {
        this.dateColor = dateColor;
    }

    public String getTitleColor() {
        return titleColor;
    }

    public String getCommentColor() {
        return commentColor;
    }

    public String getDateColor() {
        return dateColor;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getTitleNotes() {
        return titleNotes;
    }

    public void setTitleNotes(String titleNotes) {
        this.titleNotes = titleNotes;
    }

    public String getTimeNotesCreated() {
        return timeNotesCreated;
    }

    public void setTimeNotesCreated(String timeNotesCreated) {
        this.timeNotesCreated = timeNotesCreated;
    }

    public String getCommentNotes() {
        return commentNotes;
    }

    public void setCommentNotes(String commentNotes) {
        this.commentNotes = commentNotes;
    }

    public String getDescriptionNotes() {
        return descriptionNotes;
    }

    public void setDescriptionNotes(String descriptionNotes) {
        this.descriptionNotes = descriptionNotes;
    }

    public String getImageNotes() {
        return imageNotes;
    }

    public void setImageNotes(String imageNotes) {
        this.imageNotes = imageNotes;
    }

    public String getColourNotes() {
        return colourNotes;
    }

    public void setColourNotes(String colourNotes) {
        this.colourNotes = colourNotes;
    }

    @Override
    public String toString() {
        return "Notes{" +
                "titleNotes='" + titleNotes + '\'' +
                ", timeNotesCreated=" + timeNotesCreated +
                ", commentNotes='" + commentNotes + '\'' +
                ", descriptionNotes='" + descriptionNotes + '\'' +
                ", imageNotes='" + imageNotes + '\'' +
                ", colourNotes='" + colourNotes + '\'' +
                '}';
    }

    public String getNoteID() {
        return noteID;
    }

    public void setNoteID(String noteID) {
        this.noteID = noteID;
    }
}
