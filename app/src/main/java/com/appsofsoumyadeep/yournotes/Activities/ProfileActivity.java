package com.appsofsoumyadeep.yournotes.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.google.android.material.textfield.TextInputEditText;
import com.appsofsoumyadeep.yournotes.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {

    private CircleImageView circleImageView;
    private TextInputEditText diplayName;
    private Button updateprofieButton;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        circleImageView = (CircleImageView) findViewById(R.id.profilePic);
        diplayName = (TextInputEditText) findViewById(R.id.displayNameEditText);
        updateprofieButton = (Button) findViewById(R.id.updateProfileButtom);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        updateprofieButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }
}