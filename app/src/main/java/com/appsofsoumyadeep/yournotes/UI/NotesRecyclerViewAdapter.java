package com.appsofsoumyadeep.yournotes.UI;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.appsofsoumyadeep.yournotes.Model.Notes;
import com.appsofsoumyadeep.yournotes.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

public class NotesRecyclerViewAdapter extends FirestoreRecyclerAdapter<Notes,
        NotesRecyclerViewAdapter.NoteViewHolder> {

    private NoteListener noteListener;

    public NotesRecyclerViewAdapter(@NonNull FirestoreRecyclerOptions<Notes> options, NoteListener noteListener) {
        super(options);
        this.noteListener = noteListener;
    }

    @Override
    protected void onBindViewHolder(@NonNull NoteViewHolder holder, int position, @NonNull Notes notes) {

        holder.textTitleMain.setText(notes.getTitleNotes());
        holder.textDateMain.setText(notes.getTimeNotesCreated());
        holder.textCommentMain.setText(notes.getCommentNotes());

        if (!holder.textCommentMain.getText().toString().equals("")) {
            holder.textCommentMain.setVisibility(View.VISIBLE);
        }

        GradientDrawable gradientDrawable = (GradientDrawable) holder.noteHolder.getBackground();
        gradientDrawable.setColor(Color.parseColor(notes.getColourNotes()));

        holder.textTitleMain.setTextColor(Color.parseColor(notes.getTitleColor()));
        holder.textCommentMain.setTextColor(Color.parseColor(notes.getCommentColor()));
        holder.textDateMain.setTextColor(Color.parseColor(notes.getDateColor()));

        if (notes.getImageNotes() != null) {

            Glide.with(holder.imageView.getContext())
                    .load(notes.getImageNotes())
                    .centerCrop()
                    .into(holder.imageView);
            holder.imageView.setVisibility(View.VISIBLE);

        }
        else {
            holder.imageView.setVisibility(View.GONE);
        }

    }

    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_note_holder,
                parent, false);
        return new NoteViewHolder(view);
    }

    public class NoteViewHolder extends RecyclerView.ViewHolder{

        TextView textTitleMain, textCommentMain, textDateMain;
        LinearLayout noteHolder;
        RoundedImageView imageView;

        public NoteViewHolder(@NonNull View itemView) {
            super(itemView);

            textTitleMain = (TextView) itemView.findViewById(R.id.textTitleMain);
            textCommentMain = (TextView) itemView.findViewById(R.id.textCommentMain);
            textDateMain = (TextView) itemView.findViewById(R.id.textDateMain);
            noteHolder = (LinearLayout) itemView.findViewById(R.id.noteHolder);
            imageView = (RoundedImageView) itemView.findViewById(R.id.imageNoteMain);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("NICE", "onClick: GG");
                    DocumentSnapshot snapshot = getSnapshots().getSnapshot(getAdapterPosition());
                    noteListener.viewNotes(snapshot);
                }
            });

        }

        public void swipeDeleteNote(){
            noteListener.swipeDeleteNote(getSnapshots().getSnapshot(getAdapterPosition()));
        }

    }

    @Override
    public void updateOptions(@NonNull FirestoreRecyclerOptions<Notes> options) {
        super.updateOptions(options);
    }

    public interface NoteListener{
        void viewNotes(DocumentSnapshot snapshot);
        void swipeDeleteNote(DocumentSnapshot snapshot);
    }
}
